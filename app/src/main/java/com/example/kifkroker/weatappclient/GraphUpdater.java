package com.example.kifkroker.weatappclient;

import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Kif Kroker on 1/12/2018.
 */

public class GraphUpdater
{
    public static boolean lastDayActivated = false;

    public GraphUpdater()
    {

    }

    private static ResultSet rsLastDay = null;
    public static Runnable LastDayRunnable = new Runnable() {
        @Override
        public void run() {

            try
            {
                if ((EntryActivity.instance.isGraphVisible() || EntryActivity.instance.isGraphHumDVisible()|| EntryActivity.instance.isGraphBarDVisible()|| EntryActivity.instance.isGraphLumDVisible())) {
                    EntryActivity.instance.getHandler().postDelayed(this, 600000);

                    rsLastDay = null;

                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Class.forName("com.mysql.jdbc.Driver");
                                //todo try without autoReconnect (otherwise it will fail 3 times before it stops)
                                Connection con = DriverManager.getConnection("jdbc:mysql://84.245.33.139/weather_data?autoReconnect=true&loginTimeout=500", "weat", "9ev!GR");
                                Statement st = con.createStatement();
                                //rsLastDay = st.executeQuery("SELECT * FROM sensor_readings WHERE Time >= " + ((System.currentTimeMillis() / 1000) - 86400) + " AND Time < " + ((System.currentTimeMillis() / 1000)) + " ORDER BY Time ASC");
                                rsLastDay = st.executeQuery("SELECT * FROM sensor_readings WHERE Time >= " + ((System.currentTimeMillis() / 1000) - 86400) + " ORDER BY Time ASC");


                                if (rsLastDay == null)
                                {
                                    Toast.makeText(EntryActivity.instance.getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                                }
                                else
                                {

                                    if (EntryActivity.instance.isGraphVisible())
                                    {
                                        Vector<DataPoint> points = new Vector<>();
                                        float minTemp = 5;
                                        float maxTemp = -100;
                                        while (rsLastDay.next()) {
                                            Date d = new Date(rsLastDay.getLong(7) * 1000);
                                            float temp = rsLastDay.getFloat(3);
                                            points.add(new DataPoint(d, temp));
                                            if (temp < minTemp) {
                                                minTemp = temp;
                                            } else if (temp > maxTemp) {
                                                maxTemp = temp;
                                            }

                                        }

                                        ((GraphView) EntryActivity.instance.findViewById(R.id.graph)).getViewport().setMinY(minTemp-5);
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.graph)).getViewport().setMaxY(maxTemp+5);

                                        DataPoint poinArray[] = (DataPoint[]) points.toArray(new DataPoint[0]);
                                        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(poinArray);
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.graph)).removeAllSeries();
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.graph)).addSeries(series);
                                    }
                                    else if (EntryActivity.instance.isGraphHumDVisible())
                                    {
                                        Vector<DataPoint> points = new Vector<>();
                                        while (rsLastDay.next()) {
                                            Date d = new Date(rsLastDay.getLong(7) * 1000);
                                            float hum = rsLastDay.getFloat(4);
                                            points.add(new DataPoint(d, hum));
                                        }
                                        DataPoint poinArray[] = (DataPoint[]) points.toArray(new DataPoint[0]);
                                        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(poinArray);
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.humGraphD)).removeAllSeries();
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.humGraphD)).addSeries(series);
                                    }
                                    else if (EntryActivity.instance.isGraphBarDVisible())
                                    {
                                        Vector<DataPoint> points = new Vector<>();
                                        float minBar = 1050;
                                        float maxBar = 950;
                                        while (rsLastDay.next()) {
                                            Date d = new Date(rsLastDay.getLong(7) * 1000);
                                            float bar = rsLastDay.getFloat(5)/100f;
                                            points.add(new DataPoint(d, bar));

                                            if (bar < minBar) {
                                                minBar = bar;
                                            } else if (bar > maxBar) {
                                                maxBar = bar;
                                            }
                                        }
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.barGraphD)).getViewport().setMinY(minBar-10);
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.barGraphD)).getViewport().setMaxY(maxBar+10);
                                        DataPoint poinArray[] = (DataPoint[]) points.toArray(new DataPoint[0]);

                                        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(poinArray);
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.barGraphD)).removeAllSeries();
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.barGraphD)).addSeries(series);
                                    }
                                    else if (EntryActivity.instance.isGraphLumDVisible())
                                    {
                                        Vector<DataPoint> points = new Vector<>();
                                        while (rsLastDay.next()) {
                                            Date d = new Date(rsLastDay.getLong(7) * 1000);
                                            float lum = rsLastDay.getFloat(6);
                                            points.add(new DataPoint(d, lum));
                                        }

                                        DataPoint poinArray[] = (DataPoint[]) points.toArray(new DataPoint[0]);
                                        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(poinArray);
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.lumGraphD)).removeAllSeries();
                                        ((GraphView) EntryActivity.instance.findViewById(R.id.lumGraphD)).addSeries(series);
                                    }

                                }

                            } catch (SQLException e) {
                                e.printStackTrace();
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }


            } catch(Exception e){
                e.printStackTrace();
                System.out.println(e.toString());
            }
        }

    };




}
