package com.example.kifkroker.weatappclient;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;

import java.util.Timer;

/**
 * Todo temperatuur vakje groter onderaan buttons voor menu waarin je advanced views kan openen
 */
public class EntryActivity extends AppCompatActivity {

    public static EntryActivity instance;


    DisplayMetrics metrics = new DisplayMetrics();

    private boolean graphVisible = false;
    private boolean graphHumDVisible = false;
    private boolean graphBarDVisible = false;
    private boolean graphLumDVisible = false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        instance = this;


        //Initializing the graphs
        GraphView graph = (GraphView) findViewById(R.id.graph);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(System.currentTimeMillis() - 86400000);
        graph.getViewport().setMaxX(System.currentTimeMillis());
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(30);
        graph.setTitle("Daily Temperature");
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graph.getGridLabelRenderer().setNumHorizontalLabels(2);
        graph.getGridLabelRenderer().setHumanRounding(false);



        GraphView graphHumD = (GraphView) findViewById(R.id.humGraphD);

        graphHumD.getViewport().setXAxisBoundsManual(true);
        graphHumD.getViewport().setMinX(System.currentTimeMillis() - 86400000);
        graphHumD.getViewport().setMaxX(System.currentTimeMillis());
        graphHumD.getViewport().setYAxisBoundsManual(true);
        graphHumD.getViewport().setMinY(0);
        graphHumD.getViewport().setMaxY(100);
        graphHumD.setTitle("Daily Humidity");
        graphHumD.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graphHumD.getGridLabelRenderer().setNumHorizontalLabels(2); // only 4 because of the space
        graphHumD.getGridLabelRenderer().setHumanRounding(false);



        GraphView graphBarD = (GraphView) findViewById(R.id.barGraphD);

        graphBarD.getViewport().setXAxisBoundsManual(true);
        graphBarD.getViewport().setMinX(System.currentTimeMillis() - 86400000);
        graphBarD.getViewport().setMaxX(System.currentTimeMillis());
        graphBarD.getViewport().setYAxisBoundsManual(true);
        graphBarD.getViewport().setMinY(950);
        graphBarD.getViewport().setMaxY(1050);
        graphBarD.setTitle("Daily Pressure");
        graphBarD.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graphBarD.getGridLabelRenderer().setNumHorizontalLabels(2); // only 4 because of the space
        graphBarD.getGridLabelRenderer().setHumanRounding(false);

        GraphView graphLumD = (GraphView) findViewById(R.id.lumGraphD);

        graphLumD.getViewport().setXAxisBoundsManual(true);
        graphLumD.getViewport().setMinX(System.currentTimeMillis() - 86400000);
        graphLumD.getViewport().setMaxX(System.currentTimeMillis());
        graphLumD.getViewport().setYAxisBoundsManual(true);
        graphLumD.getViewport().setMinY(0);
        graphLumD.getViewport().setMaxY(200);
        graphLumD.setTitle("Daily Luminosity");
        graphLumD.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graphLumD.getGridLabelRenderer().setNumHorizontalLabels(2); // only 4 because of the space
        graphLumD.getGridLabelRenderer().setHumanRounding(false);



        //Update metrics
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        //Temperature slider handler
        Button button = (Button) findViewById(R.id.button);
        findViewById(R.id.main_temp_icon).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_MOVE:
                        float pos = event.getRawY()/metrics.heightPixels;
                        float posNextButton = 0.45f;
                        if(pos<0.26)
                        {
                            pos=0.26f;

                            graph.setVisibility(View.INVISIBLE);
                            graphVisible=false;
                            button.setBackgroundResource(android.R.color.holo_blue_dark);

                            //turn the DayRunnable Thread off
                            if( !graphVisible && !graphHumDVisible && !graphBarDVisible && !graphLumDVisible)
                            {
                                GraphUpdater.lastDayActivated = false;
                            }

                        }
                        else if(pos>0.26f)
                        {
                            //enable graph
                            graph.setVisibility(View.VISIBLE);
                            graphVisible=true;
                            button.setBackgroundResource(android.R.color.holo_blue_light);
                            //do update once, and start updating
                            if(!GraphUpdater.lastDayActivated)
                            {
                                GraphUpdater.lastDayActivated=true;
                                handler.postDelayed(GraphUpdater.LastDayRunnable, 123);
                            }
                        }

                        if(pos>0.45)
                        {
                            posNextButton = pos;
                        }
                        if (pos>0.61)
                        {
                            pos=0.61f;
                            posNextButton = 0.61f;
                            //button.setBackgroundResource(android.R.color.holo_blue_bright);
                        }

                        Guideline guideLine = (Guideline) findViewById(R.id.guideline19Hp);
                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =pos;
                        guideLine.setLayoutParams(params);


                        guideLine = (Guideline) findViewById(R.id.guideline38Hp);
                        params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =posNextButton;
                        guideLine.setLayoutParams(params);
                        break;
                }
                return true;
            }
        });


        //Humidity slider handler
        Button button2 = (Button) findViewById(R.id.button2);
        findViewById(R.id.main_hum_icon).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //int x = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_MOVE:
                        //System.out.println("uhm :"+event.getRawY()/metrics.heightPixels);
                        float pos = event.getRawY()/metrics.heightPixels;
                        float posNextButton = 0.64f;
                        if(pos<0.45)
                        {
                            pos=0.45f;

                            graphHumD.setVisibility(View.INVISIBLE);
                            graphHumDVisible = false;
                            button2.setBackgroundResource(android.R.color.holo_blue_dark);

                            if( !graphVisible && !graphHumDVisible && !graphBarDVisible && !graphLumDVisible)
                            {
                                GraphUpdater.lastDayActivated = false;
                            }
                        }
                        else if(pos>0.45f)
                        {
                            //enable graph
                            graphHumD.setVisibility(View.VISIBLE);
                            graphHumDVisible = true;
                            button2.setBackgroundResource(android.R.color.holo_blue_light);
                            if(!GraphUpdater.lastDayActivated)
                            {
                                GraphUpdater.lastDayActivated=true;
                                handler.postDelayed(GraphUpdater.LastDayRunnable, 123);
                            }
                        }

                        if(pos>0.64)
                        {
                            posNextButton = pos;
                        }
                        if (pos>0.8)
                        {
                            pos=0.8f;
                            posNextButton = 0.8f;
                        }
                        Guideline guideLine = (Guideline) findViewById(R.id.guideline38Hp);
                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =pos;
                        guideLine.setLayoutParams(params);

                        guideLine = (Guideline) findViewById(R.id.guideline57Hp);
                        params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =posNextButton;
                        guideLine.setLayoutParams(params);
                        break;
                }
                return true;
            }
        });


        //Pressure slider handler
        Button button3 = (Button) findViewById(R.id.button3);
        findViewById(R.id.main_bar_icon).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //int x = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_MOVE:
                        float pos = event.getRawY()/metrics.heightPixels;
                        if(pos<0.64)
                        {
                            pos=0.64f;

                            graphBarD.setVisibility(View.INVISIBLE);
                            graphBarDVisible = false;
                            button3.setBackgroundResource(android.R.color.holo_blue_dark);

                            if( !graphVisible && !graphHumDVisible && !graphBarDVisible && !graphLumDVisible)
                            {
                                GraphUpdater.lastDayActivated = false;
                            }
                        }
                        else if(pos>0.64f)
                        {   //enable graph
                            graphBarD.setVisibility(View.VISIBLE);
                            graphBarDVisible = true;
                            button3.setBackgroundResource(android.R.color.holo_blue_light);
                            if(!GraphUpdater.lastDayActivated)
                            {
                                GraphUpdater.lastDayActivated=true;
                                handler.postDelayed(GraphUpdater.LastDayRunnable, 123);
                            }
                        }
                        if (pos>0.8)
                        {
                            pos=0.8f;
                        }
                        Guideline guideLine = (Guideline) findViewById(R.id.guideline57Hp);
                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =pos;
                        guideLine.setLayoutParams(params);

                        break;
                }
                return true;
            }
        });


        //Luminosity slider handler
        Button button4 = (Button) findViewById(R.id.button4);
        findViewById(R.id.main_lum_icon).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //int x = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_MOVE:
                        float pos = event.getRawY()/metrics.heightPixels;
                        float posNextButton = 0.42f;
                        if(pos>0.61)
                        {
                            pos=0.61f;

                            graphLumD.setVisibility(View.INVISIBLE);
                            graphLumDVisible = false;
                            button3.setBackgroundResource(android.R.color.holo_blue_dark);

                            if( !graphVisible && !graphHumDVisible && !graphBarDVisible && !graphLumDVisible)
                            {
                                GraphUpdater.lastDayActivated = false;
                            }
                        }
                        else if(pos<0.61f)
                        {   //enable graph
                            graphLumD.setVisibility(View.VISIBLE);
                            graphLumDVisible = true;
                            button3.setBackgroundResource(android.R.color.holo_blue_light);
                            if(!GraphUpdater.lastDayActivated)
                            {
                                GraphUpdater.lastDayActivated=true;
                                handler.postDelayed(GraphUpdater.LastDayRunnable, 123);
                            }
                        }

                        //technically this button stays infront :/
                        if (pos<0.42f)
                        {
                            posNextButton = pos;
                            button2.setBackgroundResource(android.R.color.holo_blue_light);
                        }
                        else
                        {
                            button2.setBackgroundResource(android.R.color.holo_blue_dark);
                        }
                        if (pos<0.26f)
                        {
                            pos=0.26f;
                            posNextButton = 0.26f;
                        }
                        Guideline guideLine = (Guideline) findViewById(R.id.guideline54Hp);
                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =pos;
                        guideLine.setLayoutParams(params);

                        guideLine = (Guideline) findViewById(R.id.guideline35Hp);
                        params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =posNextButton;
                        guideLine.setLayoutParams(params);
                        break;
                }
                return true;
            }
        });


        //About-us slider handler
        TextView aboutView = (TextView) findViewById(R.id.aboutTextView);
        findViewById(R.id.teamLogo).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //int x = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_MOVE:
                        float pos = event.getRawY()/metrics.heightPixels;
                        if(pos>0.80f)
                        {
                            pos=0.80f;
                            aboutView.setVisibility(View.INVISIBLE);
                            findViewById(R.id.saxLogo).setVisibility(View.INVISIBLE);
                            button4.setBackgroundResource(android.R.color.holo_blue_dark);
                        }
                        else
                        {
                            aboutView.setVisibility(View.VISIBLE);
                            findViewById(R.id.saxLogo).setVisibility(View.VISIBLE);
                            graphLumD.setVisibility(View.INVISIBLE);
                            button4.setBackgroundResource(android.R.color.holo_blue_light);

                        }
                        if(pos<0.26f)
                        {
                            pos=0.26f;
                        }
                        Guideline guideLine = (Guideline) findViewById(R.id.guideline73Hp);
                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideLine.getLayoutParams();
                        params.guidePercent =pos;
                        guideLine.setLayoutParams(params);
                        break;
                }
                return true;
            }
        });


        new DBConnector().testDB();
        handler.postDelayed(SelfUpdate, 100);

    }


    private Handler handler = new Handler();

    public Handler getHandler() {
        return handler;
    }

    /**
     * This function updates the data of the main view and updates the image
     */
    private Runnable SelfUpdate = new Runnable() {
        @Override
        public void run() {
            AppCompatTextView actx = instance.findViewById(R.id.main_temp_value2);
            actx.setText(String.format("%.1f", Float.intBitsToFloat(DBConnector.temp.get()))+"\u00B0");
            actx = instance.findViewById(R.id.main_hum_value);
            actx.setText(String.format("%.1f", Float.intBitsToFloat(DBConnector.hum.get()))+"%");
            actx = instance.findViewById(R.id.main_bar_value);
            actx.setText(String.format("%.1f", Float.intBitsToFloat(DBConnector.baro.get())/100)+"");
            actx = instance.findViewById(R.id.main_lum_value);
            actx.setText(String.format("%.1f", Float.intBitsToFloat(DBConnector.lumi.get()))+"");
            TextView image = (TextView) instance.findViewById(R.id.main_temp_icon);

            if( Float.intBitsToFloat(DBConnector.hum.get()) >75 )
            {
                if( Float.intBitsToFloat(DBConnector.temp.get()) < 15 )
                {
                    //rain only
                    image.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rain)   );
                }
                else
                {
                    //intermediate
                    image.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.sunwithshowers)   );
                }
            }
            else
            {
                if( Float.intBitsToFloat(DBConnector.temp.get()) <= 0 )
                {
                    //frost
                    image.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.forst)   );
                }
                else if ( Float.intBitsToFloat(DBConnector.temp.get()) < 15 )
                {
                    //cloud
                    image.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.heavycloud)   );
                }
                else
                {
                    //sun
                    image.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.sunny)   );
                }

            }

            handler.postDelayed(this, 1000);
        }
    };


    public boolean isGraphVisible() {
        return graphVisible;
    }

    public boolean isGraphHumDVisible() {
        return graphHumDVisible;
    }

    public boolean isGraphBarDVisible() {
        return graphBarDVisible;
    }

    public boolean isGraphLumDVisible() {
        return graphLumDVisible;
    }
}
