package com.example.kifkroker.weatappclient;

import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Kif Kroker on 12/21/2017.
 */

public class DBConnector
{
    static AtomicInteger temp = new AtomicInteger();
    static AtomicInteger hum = new AtomicInteger();
    static AtomicInteger baro = new AtomicInteger();
    static AtomicInteger lumi = new AtomicInteger();

    public void testDB() {
        new Thread(new Runnable() {
            public void run() {
                    try {

                        Class.forName("com.mysql.jdbc.Driver");
                        Connection con = DriverManager.getConnection("jdbc:mysql://84.245.33.139/weather_data?autoReconnect=true", "weat", "9ev!GR");
                        Statement st = con.createStatement();


                        while(true)
                        {
                            ResultSet rs = st.executeQuery("SELECT * FROM sensor_readings ORDER BY PackageNumber DESC LIMIT 0, 1 ");

                            ResultSetMetaData rsmd = rs.getMetaData();
                            if(rsmd.getColumnCount()==7)
                            {

                                while (rs.next()) {
                                    temp.set(Float.floatToIntBits(rs.getFloat(3)));
                                    hum.set(Float.floatToIntBits(rs.getFloat(4)));
                                    baro.set(Float.floatToIntBits(rs.getFloat(5)));
                                    lumi.set(Float.floatToIntBits(rs.getFloat(6)));
                                }
                            }

                            Thread.sleep(5000);

                            try {
                                st.executeQuery("select version()");
                            }
                            catch(Exception exception)
                            {
                                System.out.println("Connection lost, reconnecting");
                                con = DriverManager.getConnection("jdbc:mysql://84.245.33.139/weather_data?autoReconnect=true", "weat", "9ev!GR");
                                //String result = "Database connection success\n";
                                st = con.createStatement();
                                System.out.println("Connected");
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println(e.toString());
                    }
                }
        }).start();



    }
}
